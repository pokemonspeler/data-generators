#!/usr/bin/env python3

import string
import random


def gen():
    alphabet = string.ascii_letters + string.digits + string.punctuation

    print("The alphabeth that is used is: ")
    print(alphabet)

    print()

    result = []
    passwd_lenght = 20
    passwd_amount = 10

    i = 0
    while (i < passwd_amount):
        result.append("")
        j = 0
        while (j < passwd_lenght):
            result[i] += random.choice(alphabet)
            j += 1
        i += 1

    print("The " + str(len(result)) + " generated passwords are:")

    i = 0
    while (i < len(result)):
        print(" - " + result[i])
        i += 1


if __name__ == '__main__':
    gen()
