#!/usr/bin/env python3

# import json
# import random

# random.randint(x, y)

# USER CONFIG


width = 30
height = 20


# END OF USER CONFIG
########################################################################################################################
########################################################################################################################
########################################################################################################################

def room(classification, x, y, width, height):
    classification_switch = {
        1: "1 Star",
        2: "2 Stars",
        3: "3 Stars",
        4: "4 Stars",
        5: "5 Stars",
    }

    return {
               "Classification": classification_switch.get(classification),
               "AreaType": "Room",
               "Position": x + ", " + y,
               "Dimension": width + ", " + height
           },


for y in range(1, height + 1):
    for x in range(1, width + 1):


        print(str(x) + ":" + str(y))
