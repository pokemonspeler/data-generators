#!/usr/bin/env python

import random
import json
import string
import os

randint = lambda x, y: str(random.randint(x, y))
random_letter = lambda: random.choice(string.ascii_uppercase)

postal_code = lambda: randint(1000, 9999) + ' ' + random_letter() + random_letter()
phone_number = lambda: '(0' + randint(10, 99) + ') ' + randint(100, 999) + ' ' + randint(10, 99) + ' ' + randint(10, 99)
email = lambda fn, ln: (fn + '.' + ".".join(last.split()) + '@mail.nl').lower()

first_names = ['Daan', 'Anna', 'Noah', 'Emma', 'Sem', 'Tess', 'Lucas', 'Sophie', 'Jesse', 'Julia', 'Finn', 'Zoë',
               'Milan', 'Evi', 'Max', 'Mila', 'Levi', 'Sara', 'Luuk', 'Eva', 'Bram', 'Fenna', 'Mees', 'Lotte', 'Liam',
               'Lisa', 'Thijs', 'Nora', 'Adam', 'Lynn', 'Sam', 'Liv', 'Thomas', 'Sarah', 'Ruben', 'Fleur', 'Julian',
               'Lauren', 'Lars', 'Olivia', 'Benjamin', 'Roos', 'Teun', 'Yara', 'Noud', 'Saar', 'Mats', 'Maud', 'Tim',
               'Noor', 'James', 'Lieke', 'Hugo', 'Noa', 'Vince', 'Isa', 'Gijs', 'Nova', 'Stijn', 'Nina', 'Jan', 'Elin',
               'David', 'Milou', 'Jayden', 'Sofie', 'Luca', 'Liz', 'Jack', 'Sofia', 'Boaz', 'Femke', 'Jens', 'Esmee',
               'Dex', 'Sanne', 'Siem', 'Emily', 'Mason', 'Naomi', 'Sven', 'Jasmijn', 'Tijn', 'Hannah', 'Cas', 'Amy',
               'Olivier', 'Ella', 'Floris', 'Lizzy', 'Ryan', 'Vera', 'Guus', 'Jill', 'Senn', 'Feline', 'Tom', 'Lina',
               'Willem', 'Elise']
last_names = ['de Jong', 'Jansen', 'de Vries', 'van der Berg', 'van Dijk', 'Bakker', 'Janssen', 'Visser', 'Smit',
              'Meyer', 'de Boer', 'Mulder', 'de Groot', 'Bos', 'Vos', 'Peters', 'Hendriks', 'van Leeuwen', 'Dekker',
              'Brouwer', 'de Wit', 'Dijkstra', 'Smits', 'de Graaf', 'van der Meer', 'van der Linden', 'Kok', 'Jacobs',
              'de Haan', 'Vermeulen', 'van den Heuvel', 'van der Veen', 'van den Broek', 'de Bruijn', 'de Bruin',
              'van der Heijden', 'Schouten', 'van Beek', 'Willems', 'van Vliet', 'van der Ven', 'Hoekstra', 'Maas',
              'Verhoeven', 'Koster', 'van Dam', 'van de Wal', 'Prins', 'Blom', 'Huisman', 'Peeters', 'de Jonge',
              'Kuipers', 'van Veen', 'Post', 'Kuiper', 'Veenstra', 'Kramer', 'van den Brink', 'Scholten', 'van Wijk',
              'Postma', 'Martens', 'Vink', 'de Ruiter', 'Timmermans', 'Groen', 'Gerritsen', 'Jonker', 'van Loon',
              'Boer', 'van der Velde', 'Willemsen', 'Smeets', 'de Lange', 'de Vos', 'Bosch', 'van Dongen', 'Schipper',
              'de Koning', 'van der Laan', 'Koning', 'van den Velden', 'Driessen', 'van Doorn', 'Hermans', 'Evers',
              'van den Bosch', 'van der Meulen', 'Hofman', 'Bosman', 'Wolters', 'Sanders', 'van der Horst', 'Mol',
              'Kuijpers', 'Molenaar', 'van de Pol', 'de Leeuw', 'Verbeek']
street_names = ['Kerkstraat', 'Schoolstraat', 'Molenstraat', 'Dorpsstraat', 'Molenweg', 'Julianastraat', 'Parallelweg',
                'Nieuwstraat', 'Wilhelminastraat', 'Sportlaan', 'Industrieweg', 'Beatrixstraat', 'Kastanjelaan',
                'Stationsweg', 'Eikenlaan', 'Marktstraat', 'Prins Bernhardstraat', 'Emmastraat', 'Beukenlaan',
                'Kerkstraat', 'High Street', 'Station Road', 'Main Street', 'Park Road', 'Church Road', 'Church Street',
                'London Road', 'Victoria Road', 'Green Lane', 'Manor Road', 'Church Lane', 'Park Avenue', 'The Avenue',
                'The Crescent', 'Queens Road', 'New Road', 'Grange Road', 'Kings Road', 'Kingsway', 'Windsor Road',
                'Birkenweg', 'Wiesenweg', 'Mühlenweg', 'Second Street', 'Third Street', 'First Street', 'Fourth Street',
                'Park Street', 'Fifth Street', 'Main Street', 'Sixth Street', 'Oak Street', 'Seventh Street',
                'Pine Street', 'Maple Street', 'Cedar Street', 'Eighth Street', 'Elm Street', 'View Street',
                'Washington Street', 'Ninth Street', 'Lake Street', 'Hill Street', 'Main Street', 'Central Street',
                'Molenstraat', 'Kerkstraat', 'Nieuwstraat', 'Schoolstraat', 'Veldstraat', 'Kerkstraat', 'Molenstraat',
                'Stationstraat', 'Schoolstraat', 'Kapelstraat']
cities = ['Amsterdam', 'Rotterdam', 'Den Haag', 'Utrecht', 'Eindhoven', 'Tilburg', 'Groningen', 'Almere', 'Breda',
          'Nijmegen', 'Enschede', 'Apeldoorn', 'Haarlem', 'Amersfoort', 'Zaanstad', 'Arnhem', 'Haarlemmermeer',
          "'s Hertogenbosch", 'Zoetermeer', 'Zwolle', 'Maastricht', 'Leiden', 'Dordrecht', 'Ede', 'Emmen',
          'Westland', 'Venlo', 'Delft', 'Deventer', 'Leeuwarden', 'Alkmaar', 'Sittar', 'Helmond', 'Heerlen',
          'Hilversum', 'Oss', 'Amstelveen', 'Súdwest-Fryslân', 'Hengelo', 'Purmerend', 'Roosendaal', 'Schiedam',
          'Lelystad', 'Alphen aan den Rijn', 'Leidschendam-Voorburg', 'Almelo', 'Spijkenisse', 'Hoorn', 'Gouda',
          'Vlaardingen', 'Assen', 'Bergen op Zoom', 'Capelle aan den IJssel', 'Veenendaal', 'Katwijk', 'Zeist',
          'Nieuwegein', 'Roermond', 'Den Helder', 'Doetinchem', 'Hoogeveen', 'Terneuzen', 'Middelburg']

provinces = ['Groningen', 'Friesland', 'Drenthe', 'Overijssel', 'Flevoland', 'Gelderland', 'Utrecht', 'Noord-Holland',
             'Zuid-Holland', 'Zeeland', 'Noord-Brabant', 'Limburg']

data = []
users = 10000
output_file = os.path.abspath(os.path.join(os.path.dirname(__file__), 'output.json'))


def password(pw_len=10):
    pwlist = []

    for i in range(pw_len // 3):
        pwlist.append(random_letter().lower())
        pwlist.append(random_letter())
        pwlist.append(str(random.randrange(10)))
    for i in range(pw_len - len(pwlist)):
        pwlist.append(random_letter().lower())

    random.shuffle(pwlist)
    pwstring = "".join(pwlist)
    return pwstring


for num in range(users):
    first = random.choice(first_names)
    last = random.choice(last_names)
    username = "".join((first[:1] + last).split())

    data.append({
        'username': username,
        'first_name': first,
        'last_name': last,
        'phone_number': phone_number(),
        'email': email(fn=first, ln=last),
        'password': password(10),
        'address': {
            'nr': randint(100, 999),
            'street': random.choice(street_names),
            'postal_code': postal_code(),
            'city': random.choice(cities),
            'province': random.choice(provinces)
        }
    })

with open(output_file, 'w') as outfile:
    json.dump(data, outfile, indent=4, sort_keys=False, separators=(',', ':'))

print(f"{users} users gegenereert en geplaatst in {output_file}")
