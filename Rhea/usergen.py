#!/usr/bin/env python3

import random
import json
import csv
import string
import os

randint = lambda x, y: random.randint(x, y)
random_letter = lambda: random.choice(string.ascii_uppercase)

student_number = lambda: f"{random.choice([15, 16, 17, 18, 19, 20, 21])}{randint(100, 999)}{randint(100, 999)}"
student_email = lambda sn: (sn + '@student.hhs.nl').lower()
teacher_email = lambda fn, ln: (fn[0:1] + ln + '@hhs.nl').lower()

first_names = ['Daan', 'Anna', 'Noah', 'Emma', 'Sem', 'Tess', 'Lucas', 'Sophie', 'Jesse', 'Julia', 'Finn', 'Zoë',
               'Milan', 'Evi', 'Max', 'Mila', 'Levi', 'Sara', 'Luuk', 'Eva', 'Bram', 'Fenna', 'Mees', 'Lotte', 'Liam',
               'Lisa', 'Thijs', 'Nora', 'Adam', 'Lynn', 'Sam', 'Liv', 'Thomas', 'Sarah', 'Ruben', 'Fleur', 'Julian',
               'Lauren', 'Lars', 'Olivia', 'Benjamin', 'Roos', 'Teun', 'Yara', 'Noud', 'Saar', 'Mats', 'Maud', 'Tim',
               'Noor', 'James', 'Lieke', 'Hugo', 'Noa', 'Vince', 'Isa', 'Gijs', 'Nova', 'Stijn', 'Nina', 'Jan', 'Elin',
               'David', 'Milou', 'Jayden', 'Sofie', 'Luca', 'Liz', 'Jack', 'Sofia', 'Boaz', 'Femke', 'Jens', 'Esmee',
               'Dex', 'Sanne', 'Siem', 'Emily', 'Mason', 'Naomi', 'Sven', 'Jasmijn', 'Tijn', 'Hannah', 'Cas', 'Amy',
               'Olivier', 'Ella', 'Floris', 'Lizzy', 'Ryan', 'Vera', 'Guus', 'Jill', 'Senn', 'Feline', 'Tom', 'Lina',
               'Willem', 'Elise']
last_names = ['de Jong', 'Jansen', 'de Vries', 'van der Berg', 'van Dijk', 'Bakker', 'Janssen', 'Visser', 'Smit',
              'Meyer', 'de Boer', 'Mulder', 'de Groot', 'Bos', 'Vos', 'Peters', 'Hendriks', 'van Leeuwen', 'Dekker',
              'Brouwer', 'de Wit', 'Dijkstra', 'Smits', 'de Graaf', 'van der Meer', 'van der Linden', 'Kok', 'Jacobs',
              'de Haan', 'Vermeulen', 'van den Heuvel', 'van der Veen', 'van den Broek', 'de Bruijn', 'de Bruin',
              'van der Heijden', 'Schouten', 'van Beek', 'Willems', 'van Vliet', 'van der Ven', 'Hoekstra', 'Maas',
              'Verhoeven', 'Koster', 'van Dam', 'van de Wal', 'Prins', 'Blom', 'Huisman', 'Peeters', 'de Jonge',
              'Kuipers', 'van Veen', 'Post', 'Kuiper', 'Veenstra', 'Kramer', 'van den Brink', 'Scholten', 'van Wijk',
              'Postma', 'Martens', 'Vink', 'de Ruiter', 'Timmermans', 'Groen', 'Gerritsen', 'Jonker', 'van Loon',
              'Boer', 'van der Velde', 'Willemsen', 'Smeets', 'de Lange', 'de Vos', 'Bosch', 'van Dongen', 'Schipper',
              'de Koning', 'van der Laan', 'Koning', 'van den Velden', 'Driessen', 'van Doorn', 'Hermans', 'Evers',
              'van den Bosch', 'van der Meulen', 'Hofman', 'Bosman', 'Wolters', 'Sanders', 'van der Horst', 'Mol',
              'Kuijpers', 'Molenaar', 'van de Pol', 'de Leeuw', 'Verbeek']
users = 500
output_file = os.path.abspath(os.path.join(os.path.dirname(__file__), 'output'))
count = {
    'user': 0,
    'teacher': 0,
    'student': 0,
}
person_index = 0


def generate_users(amount: int):
    global person_index
    teachers: int = int(amount / 5)
    students: int = amount - teachers
    print(amount)
    print(teachers)
    print(students)

    for num in range(teachers):
        first: str = random.choice(first_names)
        infix: str = ''
        last: str = random.choice(last_names)
        if ' ' in last:
            infix, last = last.rsplit(' ', 1)

        person_index += 1

        yield {
            'person': {
                'Id': person_index,
                'FirstName': first,
                'Infix': infix,
                'LastName': last,
                'Email': teacher_email(fn=first, ln=last),
                'User': {
                    'Id': person_index,
                    'Teacher': {
                        'Id': person_index,
                    }
                },

            }
        }
        count['user'] += 1
        count['teacher'] += 1

    for num in range(students):
        first: str = random.choice(first_names)
        infix: str = ''
        last: str = random.choice(last_names)
        number = student_number()
        if ' ' in last:
            infix, last = last.rsplit(' ', 1)

        person_index += 1

        yield {
            'person': {
                'Id': person_index,
                'FirstName': first,
                'Infix': infix,
                'LastName': last,
                'Email': student_email(sn=number),
                'User': {
                    'Id': person_index,
                    'Student': {
                        'Id': person_index,
                        'StudentNumber': number
                    }
                },

            }
        }
        count['user'] += 1
        count['student'] += 1


def export_json(export_data, file):
    path = file + '.json'
    with open(path, 'w') as f:
        json.dump(export_data, f, indent=4, sort_keys=False, separators=(',', ': '))


def export_csv(export_data, file):
    path = file + '.csv'
    with open(path, 'w') as f:
        data_writer = csv.writer(f, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        data_writer.writerow(
            ['person_id', 'person_firstname', 'person_infix', 'person_lastname', 'person_email', 'user_id',
             'teacher_id', 'student_id', 'student_studentnumber']
        )

        for item in export_data:
            data_writer.writerow(
                [
                    item['person']['Id'], item['person']['FirstName'], item['person']['Infix'],
                    item['person']['LastName'],
                    item['person']['Email'], item['person']['User']['Id'],
                    item['person']['User'].get('Teacher', {}).get('Id', ''),
                    item['person']['User'].get('Student', {}).get('Id', ''),
                    item['person']['User'].get('Student', {}).get('StudentNumber', '')
                ]
            )


if __name__ == "__main__":
    data = [user for user in generate_users(users)]

    export_csv(data, output_file)
    export_json(data, output_file)

    print(f"""===================================
users: {count['user']}
students: {count['student']}
teachers: {count['teacher']}
placed in {output_file}.json
placed in {output_file}.csv
===================================""")
