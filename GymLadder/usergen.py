#!/usr/bin/env python3

import random
import json
import string
import os

randint = lambda x, y: random.randint(x, y)
random_letter = lambda: random.choice(string.ascii_uppercase)

postal_code = lambda: f"{randint(1000, 9999)} {random_letter()}{random_letter()}"
phone_number = lambda: f"(0{randint(10, 99)}) {randint(100, 999)} {randint(10, 99)} {randint(10, 99)}"
email = lambda fn, ln: (fn + '.' + ".".join(ln.split()) + '@mail.nl').lower()

first_names = ['Daan', 'Anna', 'Noah', 'Emma', 'Sem', 'Tess', 'Lucas', 'Sophie', 'Jesse', 'Julia', 'Finn', 'Zoë',
               'Milan', 'Evi', 'Max', 'Mila', 'Levi', 'Sara', 'Luuk', 'Eva', 'Bram', 'Fenna', 'Mees', 'Lotte', 'Liam',
               'Lisa', 'Thijs', 'Nora', 'Adam', 'Lynn', 'Sam', 'Liv', 'Thomas', 'Sarah', 'Ruben', 'Fleur', 'Julian',
               'Lauren', 'Lars', 'Olivia', 'Benjamin', 'Roos', 'Teun', 'Yara', 'Noud', 'Saar', 'Mats', 'Maud', 'Tim',
               'Noor', 'James', 'Lieke', 'Hugo', 'Noa', 'Vince', 'Isa', 'Gijs', 'Nova', 'Stijn', 'Nina', 'Jan', 'Elin',
               'David', 'Milou', 'Jayden', 'Sofie', 'Luca', 'Liz', 'Jack', 'Sofia', 'Boaz', 'Femke', 'Jens', 'Esmee',
               'Dex', 'Sanne', 'Siem', 'Emily', 'Mason', 'Naomi', 'Sven', 'Jasmijn', 'Tijn', 'Hannah', 'Cas', 'Amy',
               'Olivier', 'Ella', 'Floris', 'Lizzy', 'Ryan', 'Vera', 'Guus', 'Jill', 'Senn', 'Feline', 'Tom', 'Lina',
               'Willem', 'Elise']
last_names = ['de Jong', 'Jansen', 'de Vries', 'van der Berg', 'van Dijk', 'Bakker', 'Janssen', 'Visser', 'Smit',
              'Meyer', 'de Boer', 'Mulder', 'de Groot', 'Bos', 'Vos', 'Peters', 'Hendriks', 'van Leeuwen', 'Dekker',
              'Brouwer', 'de Wit', 'Dijkstra', 'Smits', 'de Graaf', 'van der Meer', 'van der Linden', 'Kok', 'Jacobs',
              'de Haan', 'Vermeulen', 'van den Heuvel', 'van der Veen', 'van den Broek', 'de Bruijn', 'de Bruin',
              'van der Heijden', 'Schouten', 'van Beek', 'Willems', 'van Vliet', 'van der Ven', 'Hoekstra', 'Maas',
              'Verhoeven', 'Koster', 'van Dam', 'van de Wal', 'Prins', 'Blom', 'Huisman', 'Peeters', 'de Jonge',
              'Kuipers', 'van Veen', 'Post', 'Kuiper', 'Veenstra', 'Kramer', 'van den Brink', 'Scholten', 'van Wijk',
              'Postma', 'Martens', 'Vink', 'de Ruiter', 'Timmermans', 'Groen', 'Gerritsen', 'Jonker', 'van Loon',
              'Boer', 'van der Velde', 'Willemsen', 'Smeets', 'de Lange', 'de Vos', 'Bosch', 'van Dongen', 'Schipper',
              'de Koning', 'van der Laan', 'Koning', 'van den Velden', 'Driessen', 'van Doorn', 'Hermans', 'Evers',
              'van den Bosch', 'van der Meulen', 'Hofman', 'Bosman', 'Wolters', 'Sanders', 'van der Horst', 'Mol',
              'Kuijpers', 'Molenaar', 'van de Pol', 'de Leeuw', 'Verbeek']
categories = {
    'Atletiek': [
        'Discuswerpen', 'hamerslingeren', 'hardlopen', 'hink-stap-springen', 'hoogspringen', 'hordelopen',
        'kogelslingeren', 'kogelstoten', 'marathonlopen', 'polsstokhoogspringen', 'snelwandelen', 'speerwerpen',
        'sprint', 'tienkamp', 'triatlon', 'verspringen', 'wheelen', 'zevenkamp'
    ],
    'Balsport | Doelspelen': [
        'American football', 'australian football', 'bandy', 'basketbal', 'flag football', 'floorball',
        'gaelic football',
        'handbal', 'hockey', 'horseball', 'hurling', 'ijshockey', 'inlinehockey', 'korfbal', 'krachtbal', 'lacrosse',
        'polo', 'quad Rugby', 'rolhockey', 'rolstoelbasketbal', 'rugby', 'trefbal', 'voetbal', 'waterpolo',
        'zaalhockey',
        'zaalvoetbal'
    ],
    'Balsport | Racketsport en netsporten': [
        'Badminton', 'beachvolleybal', 'bossaball', 'kaatsen', 'padel', 'racketlon', 'racquetball', 'rolstoeltennis',
        'sepak takraw', 'squash', 'tafeltennis', 'tennis', 'volleybal', 'vuistbal', 'pickleball'
    ],
    'Balsport | Honkbal e.d.': [
        'Cricket', 'honkbal(baseball)', 'kastie', 'slagbal', 'softbal'
    ],
    'Balsport | Overige': [
        'Bocce', 'boccia', 'bowlen', 'bowls', 'footgolf', 'golf', 'kaatsen', 'kegelen', 'klootschieten', 'midgetgolf',
        'petanque'
    ],
    'Behendigheidssport': [
        'Biljart', 'boogschieten', 'carambole', 'darts', 'kleiduivenschieten', 'pool', 'schietsport', 'sjoelen',
        'snooker', 'tafelvoetbal'
    ],
    'Denksport': [
        'Bridge', 'dammen', 'go', 'klaverjassen', 'mahjong', 'poker', 'schaken', 'scrabble'
    ],
    'Krachtsport': [
        'Bankdrukken', 'bodybuilding', 'gewichtheffen', 'paalwerpen', 'powerlifting', 'touwtrekken'
    ],
    'Luchtsport': [
        'Luchtsport', 'ballonvaren', 'deltavliegen', 'modelvliegen', 'parachutespringen of valschermspringen',
        'parapente', 'zeilvliegen', 'zweefvliegen'
    ],
    'Motorsport': [
        'Autosport', 'formule 1', 'karting', 'motorcross', 'motorsport', 'speedbootracen', 'trial'
    ],
    'Oriëntatiesport': [
        'Geocaching', 'oriëntatieloop'
    ],
    'Paardensport': [
        'Draverijen', 'dressuur', 'endurance', 'eventing', 'horseball', 'military', 'paardenrennen', 'polo',
        'springconcours', 'voltige', 'westernrijden', 'mennen', 'Working Equitation'
    ],
    'Rolsport': [
        'inlinehockey', 'rolhockey', 'roller derby', 'rollersoccer', 'skaten', 'skateboarden', 'skeeleren', 'Rolvoetbal'
    ],
    'Schietsport': [
        'Airsoft', 'boogschieten', 'kleiduivenschieten', 'paintball'
    ],
    'Vecht - en verdedigingssporten': [
        'Aikido', 'Amerikaans worstelen', 'American Kenpo', 'boksen', 'Braziliaans jiujitsu', 'capoeira',
        'Daito Ryu Aiki Jujutsu', 'eskrima', 'freefight', 'hankido', 'hankumdo', 'hapkido', 'jiujitsu', 'judo',
        'kajukenbo', 'karate', 'kempo', 'kendo', 'kenjutsu', 'kenpo', 'kickboksen', 'krav maga', 'kungfu',
        'mixed martial arts', 'modderworstelen', 'muay Thai', 'ninjutsu', 'nunchaku', 'do', 'pencak silat', 'schermen',
        'Shaolin', 'sumoworstelen', 'taekwondo', 'tai chi', 'tai-jutsu', 'Tang Soo Do', 'thaiboksen', 'Wing Chun',
        'worstelen'
    ],
    'Vrijetijdssport': [
        'Aerobics', 'bootcamp', 'fitness', 'inline-skaten', 'joggen', 'longboardskaten', 'skateboarden',
        'sportvissen', 'waveboard', 'pilates'
    ],
    'Wandelsport': [
        'Bergwandelen', 'langeafstandswandelen', 'nordic walking', 'snelwandelen', 'wandelen'
    ],
    'Watersport': [
        'Drakenbootvaren', 'duiken', 'golfsurfen', 'jetskiën', 'kajakken', 'kanopolo', 'kanovaren', 'longboardsurfen',
        'onderwaterhockey', 'peddelsurfen', 'roeien', 'schoonspringen', 'snorkelen', 'speervissen',
        'stand-up paddle surfing', 'synchroonzwemmen', 'vissen', 'vrijduiken', 'vinzwemmen', 'wakeboarden', 'waterpolo',
        'waterballet', 'waterskiën', 'windsurfen', 'zeilen', 'zwemmen'
    ],
    'Wielersport': [
        'Baanwielrennen', 'BMX', 'cyclobal', 'handbiken', 'mountainbiken', 'steppen', 'trial', 'veldrijden',
        'wegwielrennen'
    ],
    'Wintersport': [
        'Bandy', 'biatlon', 'bobsleeën', 'curling', 'ijshockey', 'ijszeilen', 'kunstschaatsen', 'langlaufen', 'mogul',
        'priksleeën', 'rodelen', 'rolstoelcurling', 'schaatsen', 'schansspringen', 'shorttrack', 'skeleton', 'skiën',
        'sledgehockey', 'snowboarden', 'speedskiën'
    ],
    'Extremesporten': [
        'bungeejumpen', 'freerunning', 'kitesurfen', 'klifduiken', 'klimmen', 'parkour', 'skateboarden', 'skimboarden',
        'survivalrun'
    ],
    'Anderesporten': [
        'Acrogym', 'Dansen', 'Duatlon', 'Duivensport', 'e-sport', 'fierljeppen', 'flyboarden', 'frisbee', 'rhönrad',
        'rope skipping', 'indoor cycling', 'steppen', 'survival', 'triatlon', 'trick-kiten', 'trampolinespringen',
        'turnen', 'ultimate frisbee', 'wandklimmen', 'yoga', 'paaldansen', 'crossfit']
}

users = 20
output_file = os.path.abspath(os.path.join(os.path.dirname(__file__), 'output.json'))
count = {
    'user': 0,
    'student': 0,
    'grade': 0,
    'category': 0,
    'activity': 0,
    'level': 0
}


def password(pw_len: int = 10):
    pwlist = []

    for i in range(pw_len // 3):
        pwlist.append(random_letter().lower())
        pwlist.append(random_letter())
        pwlist.append(str(random.randrange(10)))
    for i in range(pw_len - len(pwlist)):
        pwlist.append(random_letter().lower())

    random.shuffle(pwlist)
    pwstring = "".join(pwlist)
    return pwstring


def generate_users(amount: int):
    for num in range(amount):
        first: str = random.choice(first_names)
        infix: str = None
        last: str = random.choice(last_names)
        if ' ' in last:
            infix, last = last.rsplit(' ', 1)

        yield {
            'first_name': first,
            'infix': infix,
            'last_name': last,
            'phone_number': phone_number(),
            'email': email(fn=first, ln=last),
            'password': password(10),
            'role': 'ROLE_TEACHER',
            'enabled': bool(randint(0, 1)),
            'valid_till': f"{randint(1, 28)}/{randint(1, 12)}/{randint(2018, 2030)}",
            'grades': [grade for grade in generate_grades(randint(5, 8))],
            'categories': [category for category in generate_categories(randint(5, len(categories)))]
        }
        count['user'] += 1


def generate_grades(amount: int):
    for num in range(amount):
        yield {
            'name': f"groep {8 - num}",
            'students': [student for student in generate_students(randint(20, 35))]

        }
        count['grade'] += 1


def generate_students(amount: int):
    for num in range(amount):
        first = random.choice(first_names)
        infix: str = None
        last: str = random.choice(last_names)
        if ' ' in last:
            infix, last = last.rsplit(' ', 1)

        yield {
            'student_nr': f"{num}",
            'first_name': first,
            'infix': infix,
            'last_name': last,
            'comment': None
        }
        count['student'] += 1


def generate_categories(amount: int):
    gen_categories = []
    if amount > len(categories):
        amount = len(categories)

    while amount > len(gen_categories):
        gen_categories.append(random.choice(list(categories.keys())))
        gen_categories = list(set(gen_categories))

    for category in gen_categories:
        yield {
            'name': category,
            'activities': [activity for activity in generate_activities(category=category)]

        }
        count['category'] += 1


def generate_activities(category=None):
    if category is None:
        category = random.choice(list(categories.keys()))
    for activity in categories.get(category):
        yield {
            'name': activity,
            'levels': [level for level in generate_levels(randint(2, 8))]

        }
        count['activity'] += 1


def generate_levels(amount: int):
    for num in range(amount):
        yield {
            'number': f"{num}",
            'description': ''.join([random_letter().lower() for x in range(randint(50, 200))])
        }
        count['level'] += 1


if __name__ == "__main__":
    data = [user for user in generate_users(users)]

    output_file = 'E:\\_GymLadder\\Web\\assets\\generated_data\\content.json'

    with open(output_file, 'w') as outfile:
        json.dump(data, outfile, indent=4, sort_keys=False, separators=(',', ': '))

    print(f"""===================================
users: {count['user']}
students: {count['student']}
grades: {count['grade']}
categories: {count['category']}
activities: {count['activity']}
levels: {count['level']}
placed in {output_file}
===================================""")
